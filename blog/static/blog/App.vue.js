ArticleList = Vue.component('article-list', {
    data: function () { return { articles: store.state.articles } },
    template: '#article-list-template',
});

AuthorList = Vue.component('author-list', {
    data: function () { return { authors: store.state.authors } },
    template: '#author-list-template',
});

ArticleItem = Vue.component('article-item', {
    delimiters: ['[[', ']]'],
    props: ['name', 'slug', 'content'],
    template: '#article-item-template',
});

AuthorItem = Vue.component('author-item', {
    delimiters: ['[[', ']]'],
    props: ['name', 'slug'],
    template: '#author-item-template',
});