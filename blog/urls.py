from django.urls import path, include, re_path
from rest_framework import routers

from blog import views as blog_views, views

router = routers.DefaultRouter()
router.register(r'article', views.ArticleList)
router.register(r'author', views.AuthorList)

urlpatterns = [
    # paths for our app
    path('api/', include(router.urls)),
    re_path(r'', blog_views.index),
]
