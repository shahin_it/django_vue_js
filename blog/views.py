from django.shortcuts import render
from rest_framework import viewsets

from .models import Article
from .models import Author
from .serializers import ArticleSerializer, AuthorSerializer


def index(request):
    return render(request, 'blog/index.html')


def frontend(request):
    """Vue.js will take care of everything else."""
    articles = Article.objects.all()
    authors = Author.objects.all()

    data = {
        'articles': articles,
        'authors': authors,
    }

    return render(request, 'blog/template.html', data)


class ArticleList(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class AuthorList(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
